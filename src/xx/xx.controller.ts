import { Controller, Post, Body, Get, Delete } from "@nestjs/common";
import { XxService } from "./xx.service";
import { CreateXxReq as CreateXxDto } from "./dto/create-xx.dto";
import { ApiTags } from "@nestjs/swagger";
@ApiTags("Module test")
@Controller("test")
export class XxController {
  constructor(private readonly xxService: XxService) {}

  @Post("/create")
  async create(@Body() createXxDto: CreateXxDto) {
    console.log(createXxDto)
    return await this.xxService.create(createXxDto);
  }
  @Get('/get')
  async find() {
    return this.xxService.findAll();
  }
  @Get('/test')
  async test() {
    return this.xxService.test();
  }
  @Delete("/delete")
  async delete() {
    return this.xxService.delete();
  }
}
