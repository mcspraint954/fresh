import { Schema, Prop, SchemaFactory } from "@nestjs/mongoose";
import { Document, SchemaTypes, Types } from "mongoose";
import { DeliveryType } from "../store/enums/delivery-type.enum";
import { CustomerDetailsModel } from "./customer-details.model";
import { OrderStatus } from "./enums/order-status.enum";
import { PaymentMode } from "./enums/payment-mode.enum";
import { PaymentStatus } from "./enums/payment-status.enum";
//TODO: Need to work om

@Schema({ collection: "orderCollection", timestamps: true })
export class OrderModel extends Document {
  @Prop({ required: true })
  serial_number: string;

  @Prop({ required: true, type: SchemaTypes.ObjectId, ref: "CartModel" })
  cart_id: Types.ObjectId;

  @Prop({ required: true, type: CustomerDetailsModel })
  customer_details: CustomerDetailsModel;

  @Prop({ required: true, enum: Object.values(DeliveryType) })
  delivery_type: DeliveryType;

  @Prop({ required: true, enum: Object.values(OrderStatus) })
  order_status: OrderStatus;

  @Prop({ required: true, enum: Object.values(PaymentMode) })
  payment_mode: PaymentMode;

  @Prop({ enum: Object.values(PaymentStatus), default: PaymentStatus.PENDING })
  payment_status: PaymentStatus;

  @Prop({ required: true })
  sub_total: number;

  @Prop({ required: false })
  coupon_code: string;

  @Prop({ required: true })
  coupon_discount: number;

  @Prop({ required: true })
  deal_discount: number;

  @Prop({ required: true })
  total_discount: number;

  @Prop({ required: false, default: 0 })
  tax: number;

  @Prop({ default: 0 })
  delivery_charges: number;

  @Prop({ required: false, default: 0 })
  tip: number;

  @Prop({ required: true })
  grand_total: number;

  @Prop({ required: false, default: 0 })
  amount_paid: number;

  @Prop({ required: false, type: [Number], default: 0 })
  amount_paid_in_parts: number[];

  @Prop({ required: true })
  currency_code: string;

  @Prop({ required: true })
  currency_symbol: string;

  @Prop({ default: false })
  is_assigned: boolean;

  @Prop({ type: Date, default: null })
  order_assigned_on: Date | null;

  @Prop({ default: false })
  is_accepted_by_delivery_partner: boolean;

  //Foreign keys
  @Prop({ type: SchemaTypes.ObjectId, ref: "UserModel", default: null })
  partner_id: Types.ObjectId | null;

  @Prop({ type: SchemaTypes.ObjectId, ref: "CouponModel", default: null })
  coupon_id: Types.ObjectId | null;

  @Prop({ required: true, type: SchemaTypes.ObjectId, ref: "StoreModel" })
  store_id: Types.ObjectId;

  @Prop({ required: true, type: SchemaTypes.ObjectId, ref: "UserModel" })
  user_id: Types.ObjectId;
}
export const OrderSchema = SchemaFactory.createForClass(OrderModel);
