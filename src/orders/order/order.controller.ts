import { Controller } from "@nestjs/common";
import { OrderService } from "./order.service";
//TODO: 1. Create an API to get billDetailsAndVouchers
//TODO: 2. Create an API to place order
//TODO: 3. Create an API to get orderHistory
//TODO: 4. Create an API to get orderDetails
//TODO: 5. Create an API to get order status
//TODO: 6. Create an API to get cancel order
@Controller("order")
export class OrderController {
  constructor(private readonly orderService: OrderService) {}
}
